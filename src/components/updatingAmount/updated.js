import React from 'react';
import { useState } from 'react';

function UpdatedComponent(OriginalComponent) {
    function NewComponent() {

        const [amount, setAmount] = useState(5);

        const handleClick = () => {
            setAmount(amount * 2)
        };

        return <OriginalComponent handleClick={handleClick} amount={amount} />
    }
    return NewComponent;
}

export default UpdatedComponent;