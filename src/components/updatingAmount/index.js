import React from "react";
import { OriginalComponent } from "./updated";

class HocComponent extends React.Component {
  render() {
    return <OriginalComponent />;
  }
}

export default (HocComponent);
