import React from 'react';
import { useStyles } from './style';
import { Typography } from '@mui/material';
import { useEffect, useState } from 'react';



export const UsersList = () => {

    const classes = useStyles();

    const [users, setUsers] = useState ([]);

    useEffect (() => {

        const fetchUserNames = async () => {

            const response = await fetch ("https://jsonplaceholder.typicode.com/users")
            const json = await response.json();
            setUsers(json);
            console.log(json);
        }
        fetchUserNames ();
    }, []);

    const renderUserLists = users.map ((users) => {
        return (
            <div key = {users.id} >
                <Typography>
                    {users.name}
                </Typography>

            </div>
        );
    })
    return <div className={classes.root}>
        <Typography>
        {renderUserLists}
        </Typography>
    </div>
}