import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root :{
        display:"flex",
        alignItems:"center",
        justifyContent:"center"
    }
}))