import React from 'react';
import { useStyles } from './style';
import { Typography, Button } from '@mui/material';
import UpdatedComponent from '../updatingAmount/updated';

function Person2 ({amount,handleClick}) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
        <Typography>Person B has ${amount}</Typography>
        <Button onClick={handleClick}>Increase +</Button>
    </div>
    );
}

export default UpdatedComponent(Person2);