import React from 'react';
import { makeStyles } from "@mui/styles";
import { Typography } from '@mui/material';
import SecondPerson from '../../components/person_2';
import FirstPerson from '../../components/person_1';

const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: "center",
        marginTop: "10%"
    }
}))

export const Home = props => {

    const classes = useStyles();

    return <div className={classes.root}>
       <Typography>The total amount spend by the two persons</Typography>
        <FirstPerson />
        <SecondPerson />

    </div>
}