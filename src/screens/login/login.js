import React from 'react';
import { makeStyles } from "@mui/styles";
import { Typography } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: "center",
        marginTop: "10%"
    }
}))

export const Login = props => {

    const classes = useStyles();

    return <div className={classes.root}>
        <Typography variant='h4'>
            UserLists
        </Typography>
    </div>
}